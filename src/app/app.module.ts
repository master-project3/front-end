/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { 
  NbPasswordAuthStrategy, 
  NbAuthModule, 
  NbAuthJWTToken, 
  NbAuthJWTInterceptor, 
  NB_AUTH_TOKEN_INTERCEPTOR_FILTER
} from '@nebular/auth';
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbTimepickerModule,
  NbToastrModule,
  NbWindowModule
} from '@nebular/theme';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgxRruleModule} from 'ngx-rrule';
import { NbRoleProvider, NbSecurityModule } from '@nebular/security';
import { RoleProvider } from './@core/utils/role.provider';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbTimepickerModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
    }),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          register: false,
          // requestPass: false,
          resetPass: false,
          baseEndpoint: 'http://localhost:8000/api',
          refreshToken: true,
          login: {
            endpoint: '/login',
          },
          requestPass: {
            endpoint: '/reset/password',
          },
          token: {
            class: NbAuthJWTToken,
            key: 'payload.access_token',
            getter: (module: string, res: HttpResponse<any>) => {
              localStorage.setItem('user', JSON.stringify(res.body.payload.user));
              return res.body.payload.access_token;
            }
          },
          validation: {
            email: {
              required: true
            },
            password: {
              required: true,
              minLength: 6,
              maxLength: 30
            },

          }
        }),
      ],
      forms: {},
    }),
    NgbModule,
    NgxRruleModule,
    NbSecurityModule.forRoot({
      accessControl: {
        service_supervisor: {
          view: ['clients', 'jobs', 'invoice'],
          create: ['clients', 'jobs'],
          edit: ['clients', 'jobs'],
        },
        staff_manager: {
          view: ['employee', 'availability', 'time_sheet', 'jobs' , 'job_assign'],
          create: ['employee', 'availability', 'time_sheet'],
          edit: ['employee', 'availability', 'time-sheet'],
        },
        payroll_officer: {
          view: ['payroll', 'pay_now', 'payroll_history', 'invoice'],
          create: ['payroll', 'pay_now'],
          edit: ['payroll'],
        },
        admin: {
          view: ['clients', 'jobs', 'invoice', 'employee', 'availability', 'time_sheet', 'job_assign', 'payroll', 'pay_now', 'payroll_history', 'user'],
          create: ['clients', 'jobs', 'invoice', 'employee', 'availability', 'time_sheet', 'job_assign', 'payroll', 'pay_now', 'payroll_history', 'user'],
          edit: ['clients', 'jobs', 'invoice', 'employee', 'availability', 'time_sheet', 'job_assign', 'payroll', 'pay_now', 'payroll_history', 'user'],
          remove: '*',
        }
      },
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, 
      useClass: NbAuthJWTInterceptor, 
      multi: true,
    },
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER, 
      useValue: (req) => { return false; },
    },
    { provide: NbRoleProvider, useClass: RoleProvider },
  ],
  bootstrap: [AppComponent],
})

export class AppModule {
  
}
