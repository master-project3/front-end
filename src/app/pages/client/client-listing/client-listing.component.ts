import { Component, OnInit  } from '@angular/core';
import { NbDialogService } from '@nebular/theme';

import { ClientService, NavigationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';


@Component({
  selector: 'app-client-listing',
  templateUrl: './client-listing.component.html',
  styleUrls: ['./client-listing.component.scss']
})
export class ClientListingComponent implements OnInit  {

  allClients:any;
  clients:any;
  currentClient = null;
  currentIndex = -1;
  clientName = '';
  

  clientFilterNgModel:string = null;

  constructor(
    private dialogService: NbDialogService,
    private NavigationService: NavigationService,
    private clientService: ClientService
    ) {}

  ngOnInit(): void {
    this.retrieveclients();
  }

  retrieveclients(): void {
    this.clientService.getAll()
      .subscribe(
        data => {
          this.clients = data.payload;
          this.allClients = data.payload;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveclients();
    this.currentClient = null;
    this.currentIndex = -1;
  }

  setActiveClient(tutorial, index): void {
    this.currentClient = tutorial;
    this.currentIndex = index;
  }

  delete(id) {
    this.dialogService.open(DeleteConfirmationComponent)
      .onClose.subscribe(deleteValue => {
        if(deleteValue == "ok"){
          this.clientService.delete(id)
          .subscribe (
            response => {
              console.log(response);
              this.refreshList();
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  // removeAllclients(): void {
  //   this.clientService.deleteAll()
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         this.retrieveclients();
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  clientNameSearch(searchValue){
    this.clients = this.allClients.filter(item => {
      return item.name.toLowerCase().includes(searchValue.toLowerCase());
    });

  }

  searchTitle(): void {
    this.clientService.findByName(this.clientName)
      .subscribe(
        data => {
          this.clients = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  goBack(){
    this.NavigationService.back();
  }

}
