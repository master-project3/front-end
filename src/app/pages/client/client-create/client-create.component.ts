import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ClientService, NavigationService, NotificationService } from '../../../@core/utils';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.scss']
})
export class ClientCreateComponent implements AfterViewInit {


  errors: string[] = [];
  clientDetail: any = {
    name: '',
    abn:'',
    address:'',
    contact_no:'',
    url:'',
    email:'',
    status:1
  };
  submitted: boolean = false;

  constructor(
    private clientService: ClientService,
    protected cd: ChangeDetectorRef,
    private NavigationService: NavigationService,
    private notify: NotificationService,
    private router: Router
  ){
    
  }

  ngAfterViewInit(){
    this.cd.detectChanges();
  }

  saveClient(): void {
    // const data = {
    //   name: this.clientDetail.name,
    //   abn: this.clientDetail.abn
    // };

    this.clientService.create(this.clientDetail)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
          this.notify.showToast("Client Created Successfully", "Success", "success");
          this.router.navigate(['pages/clients/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        });
  }

  newclientDetail(): void {
    this.submitted = false;
    this.clientDetail = {};
  }

  goBack(){
    this.NavigationService.back();
  }

}
