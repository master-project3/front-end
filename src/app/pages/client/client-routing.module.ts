import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ClientComponent } from "./client.component";
import { ClientListingComponent } from "./client-listing/client-listing.component";
import { ClientCreateComponent } from "./client-create/client-create.component";
import { ClientEditComponent } from "./client-edit/client-edit.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: ClientComponent,
    children: [
      {
        path: "list",
        component: ClientListingComponent,
      },
      {
        path: "create",
        component: ClientCreateComponent,
      },
      {
        path: "edit/:id",
        component: ClientEditComponent,
      },
      // {
      //   path: 'layouts',
      //   component: FormLayoutsComponent,
      // },
      // {
      //   path: 'layouts',
      //   component: FormLayoutsComponent,
      // },
      // {
      //   path: 'buttons',
      //   component: ButtonsComponent,
      // },
      // {
      //   path: 'datepicker',
      //   component: DatepickerComponent,
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRoutingModule {}

export const routedComponents = [
  ClientComponent,
  ClientListingComponent,
  ClientCreateComponent,
  ClientEditComponent,
];
