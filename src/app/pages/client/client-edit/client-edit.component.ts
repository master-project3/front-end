import { Component, OnInit, AfterViewInit ,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService, NavigationService, NotificationService } from '../../../@core/utils';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements AfterViewInit {

  errors: string[] = [];
  statusValue: number = null;
  clientId = null;
  
  currentClient: any = {
    name: '',
    abn:'',
    address:'',
    contact_no:'',
    url:'',
    email:'',
    status:0
  };

  message = '';

  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private router: Router,
    protected cd: ChangeDetectorRef,
    private NavigationService: NavigationService,
    private notify: NotificationService,
    ) { }

  ngAfterViewInit(): void {
    this.message = '';
    this.clientId = this.route.snapshot.paramMap.get('id');
    this.getClient(this.clientId);
    this.cd.detectChanges();
  }

  getClient(id): void {
    this.clientService.get(id)
      .subscribe(
        data => {
          let clientData = {
            name: data?.payload?.name,
            abn: data?.payload?.abn,
            address: data?.payload?.address,
            contact_no: data?.payload?.contact_no,
            url: data?.payload?.url,
            email: data?.payload?.email || "",
            status: data?.payload?.status
          };
          this.currentClient = clientData;
          this.currentClient.status = clientData.status;
          
        },
        error => {
          console.log(error);
        });
  }


  updateClient(): void {
    this.clientService.update(this.clientId, this.currentClient)
      .subscribe(
        response => {
          console.log(response);
          // this.message = 'The Client was updated successfully!';
          this.notify.showToast("Client Updated Successfully", "Success", "success");
          this.router.navigate(['pages/clients/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        });
  }

  goBack(){
    this.NavigationService.back();
  }

}
