import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbFormFieldModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
} from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { ClientRoutingModule } from './client-routing.module';
import { FormsModule as ngFormsModule } from '@angular/forms';

import { ClientComponent } from './client.component';
import { ClientListingComponent } from './client-listing/client-listing.component';
import { ClientCreateComponent } from './client-create/client-create.component';
import { ClientEditComponent } from './client-edit/client-edit.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbFormFieldModule,
    ClientRoutingModule,
    ngFormsModule,
  ],
  declarations: [
    ClientComponent,
    ClientListingComponent,
    ClientCreateComponent,
    ClientEditComponent,
  ]
})
export class ClientModule { }
