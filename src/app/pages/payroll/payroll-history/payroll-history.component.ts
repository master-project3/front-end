import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

import { PayrollService, NavigationService, NotificationService, EmployeeService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'ngx-payroll-history',
  templateUrl: './payroll-history.component.html',
  styleUrls: ['./payroll-history.component.scss']
})
export class PayrollHistoryComponent implements OnInit {

  allPayrolls:any;
  payrolls:any = [];

  employeeFilterNgModel = null;
  employees = [];

  constructor(
    private dialogService: NbDialogService,
    private navigationService: NavigationService,
    private payrollService: PayrollService,
    private employeeService: EmployeeService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router,
  ) { }

  ngOnInit() {
    // this.getPayrollHistory();
    this.getAllEmployees();
  }

  getAllEmployees(){
    this.employeeService.getAll().subscribe(
      response => {
        // console.log(response.payload);
        this.employees = response.payload;
        
      },
      error => {
        // console.log(error);
        this.notify.showToast(error.message, "Error", "danger");

      }
    );
  }
    

  getPayrollHistory(){
    let selectedEmployee = this.employeeFilterNgModel;

    this.payrollService.getPayrollHistory(selectedEmployee).subscribe(
      response => {
        console.log(response.payload);
        this.payrolls = response.payload;
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    );
  }

  goBack(){
    this.navigationService.back();
  }


}
