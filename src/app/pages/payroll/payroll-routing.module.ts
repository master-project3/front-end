import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PayrollComponent } from "./payroll.component";
import { PayrollListingComponent } from "./payroll-listing/payroll-listing.component";
import { PayNowComponent } from "./pay-now/pay-now.component";
import { PayrollHistoryComponent } from "./payroll-history/payroll-history.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: PayrollComponent,
    children: [
      {
        path: "list",
        component: PayrollListingComponent,
      },
      {
        path: "pay",
        component: PayNowComponent,
      },
      {
        path: "history",
        component: PayrollHistoryComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayrollRoutingModule {}

export const routedComponents = [
    PayrollComponent,
    PayrollListingComponent,
    PayNowComponent,
    PayrollHistoryComponent,
  ];