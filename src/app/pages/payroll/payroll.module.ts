import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe, UpperCasePipe } from '@angular/common';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbTimepickerModule,
  NbSpinnerModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { PayrollRoutingModule } from './payroll-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';

import { PayrollComponent } from './payroll.component';
import { PayrollListingComponent } from './payroll-listing/payroll-listing.component';
import { PayNowComponent } from './pay-now/pay-now.component';
import { PayrollHistoryComponent } from './payroll-history/payroll-history.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTimepickerModule,
    PayrollRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
    NbFormFieldModule,
    NbSpinnerModule,
  ],
  declarations: [
    PayrollComponent,
    PayrollListingComponent,
    PayNowComponent,
    PayrollHistoryComponent,
  ],
  providers:[
    DatePipe,
    CurrencyPipe,
    UpperCasePipe,
  ]
})

export class PayrollModule { }
