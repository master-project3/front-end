import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { NotificationService, PayrollService } from '../../../@core/utils';

@Component({
  selector: 'app-pay-now',
  templateUrl: './pay-now.component.html',
  styleUrls: ['./pay-now.component.scss']
})
export class PayNowComponent implements OnInit {

  employeeName:string = '';
  payId:number = 0;
  payAmt:number = 0;

  loading: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private payrollService: PayrollService,
    private notify: NotificationService,
  ) { }

  ngOnInit() {
    this.employeeName = this.activatedRoute.snapshot.paramMap.get('empName');
    this.payId = Number(this.activatedRoute.snapshot.paramMap.get('payId'));
    this.payAmt = Number(this.activatedRoute.snapshot.paramMap.get('payAmt'));
    // this.payrollService.get(this.payId).subscribe(
    //   response=> {
    //     console.log(response);
    //   },
    //   error => {
    //     this.notify.showToast(error.message, "Error", "danger");
    //   }
    // )
  }

  pay(){
    this.loading = true;
    this.payrollService.payByPayslidId({"payslip_id" : this.payId}).subscribe(
      response => {
        this.notify.showToast(`Amount of ${this.payAmt} paid to ${this.employeeName} successfully `, "Success", "success");
        this.loading = false;
        setTimeout(() => this.router.navigate(['pages/payroll/list']), 2000);
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
        this.loading = true;
      }
    )
  }

}
