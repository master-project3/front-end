import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-payroll',
  template:`
  <router-outlet></router-outlet>
  `,
})
export class PayrollComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
