import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {

  menu:any; 

  constructor(){
    this.processMenu();
  }

  processMenu(){

    // console.log(MENU_ITEMS);

    for(let m of MENU_ITEMS )
    {
      if(m.data){
        // console.log(m);
        let prepared = m.data;
        if (prepared.roles && prepared.roles.length > 0) {
          let user = JSON.parse(localStorage.getItem('user'));
          const role = user?.roles[0]?.name;
          let role_name;
          if(role == 'admin') role_name = "admin";
          if(role == 'service-supervisor') role_name = "service_supervisor";
          if(role == 'staff-manager') role_name = "staff_manager";
          if(role == 'payroll-officer') role_name = "payroll_officer";
  
          if (prepared.roles.indexOf(role_name) === -1) {
            m.hidden = true;
          }
          if(m.children){
            for(let c of m.children ){
              if(c.data){
                let childData = c.data;
                if (childData.roles.indexOf(role_name) === -1) {
                  c.hidden = true;
                }
              }
            }
          }
        }
      }
    }

    this.menu = MENU_ITEMS;
    console.log(this.menu);
  }



  


}
