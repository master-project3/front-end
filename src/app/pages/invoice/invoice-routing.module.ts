import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { InvoiceComponent } from "./invoice.component";
import { InvoiceListingComponent } from "./invoice-listing/invoice-listing.component";

const routes: Routes = [
  {
    path: "",
    component: InvoiceComponent,
    children: [
      {
        path: "list",
        component: InvoiceListingComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoiceRoutingModule {}

export const routedComponents = [
    InvoiceComponent,
    InvoiceListingComponent,
  ];