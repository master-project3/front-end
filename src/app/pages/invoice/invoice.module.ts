import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe, UpperCasePipe } from '@angular/common';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbTimepickerModule,
  NbSpinnerModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';

import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';

import { InvoiceComponent } from './invoice.component';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { InvoiceListingComponent } from './invoice-listing/invoice-listing.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTimepickerModule,
    InvoiceRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
    NbFormFieldModule,
    NbSpinnerModule,
  ],
  declarations: [
    InvoiceComponent,
    InvoiceListingComponent,
  ],
  providers:[
    DatePipe,
    CurrencyPipe,
    UpperCasePipe
  ]
})
export class InvoiceModule { }
