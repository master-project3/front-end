import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

import { InvoiceService, NavigationService, NotificationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'ngx-invoice-listing',
  templateUrl: './invoice-listing.component.html',
  styleUrls: ['./invoice-listing.component.scss']
})
export class InvoiceListingComponent implements OnInit {

  allInvoices:any;
  invoices:any = [];

  loading:boolean = false;

  constructor(
    private dialogService: NbDialogService,
    private navigationService: NavigationService,
    private invoiceService: InvoiceService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getInvoiceHistory();
  }

  getInvoiceHistory(){
    this.loading = true;
    this.invoiceService.getAll().subscribe(
      response => {
        console.log(response.payload);
        this.invoices = response.payload;
        this.loading = false;
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
        this.loading = false;
      }
    );
  }

  refreshData(){
    this.getInvoiceHistory();
  }

  goBack(){
    this.navigationService.back();
  }

}
