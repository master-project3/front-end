import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NbThemeService } from '@nebular/theme';
import { AdminUserService, CurrentUserService, NotificationService, ReportService } from '../../@core/utils';

@Component({
  selector: 'ngx-ecommerce',
  styleUrls: ['./e-commerce.component.scss'],
  templateUrl: './e-commerce.component.html',
})
export class ECommerceComponent {

  loading:boolean = false;
  dashboardData:any = {};
  public userData:any;
  transactions:any = [];

  pieOptions: any = {};
  areaPieOptions:any = {};
  multiXAxisOptions:any = {};
  themeSubscription: any;

  colors:any;
  echarts:any;
  current_role:string = null;

  constructor(
    private theme: NbThemeService,
    private reportService: ReportService,
    private notify: NotificationService,
    private adminUserService: AdminUserService,
    private cus: CurrentUserService,
    private router: Router,
    ) {
    this.userData = JSON.parse(localStorage.getItem('user'));
    this.current_role = this.cus.getRole();
  }




  ngAfterViewInit() {

    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      this.colors = config.variables;
      this.echarts = config.variables.echarts;
    });

    this.getAllData();

  }

  navigateMenu(path){
    this.router.navigate([path]);
  }

  getAllData(){
    this.loading = true;
    
    // For Account Summary PieChart
    this.reportService.getAll().subscribe(
      response => {
        let reportData = response?.payload;
        this.dashboardData = response?.payload;
        this.pieOptions = {
          backgroundColor: this.echarts.bg,
          color: [this.colors.warningLight, this.colors.infoLight, this.colors.dangerLight, this.colors.successLight, this.colors.primaryLight],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : $ {c} ({d}%)',
          },
          legend: {
            orient: 'vertical',
            left: 'left',
            data: ['Total Balance', 'Received from clients', 'To be received From clients', 'Paid to employees', 'To be paid to employees'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          series: [
            {
              name: 'Accounts',
              type: 'pie',
              radius: '80%',
              center: ['50%', '50%'],
              data:[
                { value:reportData?.account_report?.balance , name: 'Total Balance'},
                { value:reportData?.account_report?.total_received_from_client , name: 'Received from clients'},
                { value:reportData?.account_report?.total_pending_receive_client , name: 'To be received From clients'},
                { value:reportData?.account_report?.total_paid_to_employee , name: 'Paid to employees'},
                { value:reportData?.account_report?.total_unpaid_employee , name: 'To be paid to employees'}
              ],
              itemStyle: {
                emphasis: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: echarts.itemHoverShadowColor,
                },
              },
              label: {
                normal: {
                  textStyle: {
                    color: echarts.textColor,
                  },
                },
              },
              labelLine: {
                normal: {
                  lineStyle: {
                    color: echarts.axisLineColor,
                  },
                },
              },
            },
          ],
        }; 


        // For Jobs Summary Area PieChart
        this.areaPieOptions = {
          title: {
            text: 'All Jobs Summary',
            // subtext: 'Mocking Data',
            x: 'center'
          },
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c} ({d}%)'
          },
          legend: {
            x: 'center',
            y: 'bottom',
            data: ['Total Jobs', 'Accepted Jobs', 'Pending Jobs', 'Assigned Jobs', 'Unassigned Jobs']
          },
          calculable: true,
          series: [
            {
              name: 'area',
              type: 'pie',
              radius: [30, 110],
              roseType: 'area',
              data: [
                { value: reportData?.job_report?.total_jobs, name: 'Total Jobs' },
                { value: reportData?.job_report?.total_accepted_jobs, name: 'Accepted Jobs' },
                { value: reportData?.job_report?.total_pending_jobs, name: 'Pending Jobs' },
                { value: reportData?.job_report?.total_assigned_jobs, name: 'Assigned Jobs' },
                { value: reportData?.job_report?.total_unassigned_job, name: 'Unassigned Jobs' }
              ]
            }
          ]
        };

        // For Invoice & Payroll Line chart
        let x1Date = [], x1Value = [], x2Date = [], x2Value = [];

        for(let data of reportData?.daily_report?.daily_invoice_amount_received){
          x1Date.push(data['date']);
          x1Value.push(data['total_received']);
        }

        for(let data of reportData?.daily_report?.daily_money_paid_to_employee){
          x2Date.push(data['date']);
          x2Value.push(data['total_received']);
        }

        this.multiXAxisOptions = {
          backgroundColor: echarts.bg,
          color: [this.colors.danger, this.colors.primary, this.colors.info],
          tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b} : {c}',
          },
          legend: {
            left: 'left',
            data: ['Line 1', 'Line 2'],
            textStyle: {
              color: echarts.textColor,
            },
          },
          xAxis: [
            {
              type: 'category',
              data: x1Date,
              axisTick: {
                alignWithLabel: true,
              },
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          yAxis: [
            {
              type: 'log',
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              splitLine: {
                lineStyle: {
                  color: echarts.splitLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true,
          },
          series: [
            {
              name: 'Line 1',
              type: 'line',
              data: x1Value,
            },
            {
              name: 'Line 2',
              type: 'line',
              data: x2Value,
            },
          ],
        };

        this.loading = false;
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
        this.loading = false;
      }
    );

      this.adminUserService.getMAccountTransactions().subscribe(
        response => {
          this.transactions = response?.message?.items || [];
          if(this.transactions.length > 0){
            this.transactions = this.transactions.filter(x => x.subTransactionType == "GatewayDirectCredit" || x.subTransactionType == "GatewayDirectDebit")
          }
          console.log(this.transactions);
        },
        error => {
          console.log(error.message);
        }
      )
  }

  ngOnDestroy(): void {
    
  }


}
