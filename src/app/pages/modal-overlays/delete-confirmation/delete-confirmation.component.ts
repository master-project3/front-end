import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-dialog-delete-prompt',
  templateUrl: 'delete-confirmation.component.html',
  styleUrls: ['delete-confirmation.component.scss'],
})
export class DeleteConfirmationComponent {

  constructor(protected ref: NbDialogRef<DeleteConfirmationComponent>) {}

  delete(val) {
    this.ref.close(val);
  }
}
