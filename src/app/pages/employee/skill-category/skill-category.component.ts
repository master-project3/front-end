import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { EmployeeService, NavigationService, NotificationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-skill-category',
  templateUrl: './skill-category.component.html',
  styleUrls: ['./skill-category.component.scss']
})
export class SkillCategoryComponent implements OnInit {

  skillCreateForm: FormGroup;
  skillEditForm: FormGroup;
  currentSkillId = null;

  addFormActive:boolean = false;
  editFormActive:boolean = false;

  skills:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private navigationService: NavigationService,
    private employeeService: EmployeeService,
    private notify: NotificationService,
  ) { }

  ngOnInit() {
    this.skillCreateForm = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required,
      ]),
    }); 
    this.skillEditForm = this.formBuilder.group({
      name: new FormControl('', [
        Validators.required,
      ]),
    }); 

    this.retrieveSkills();
  }
  

  retrieveSkills(){
    this.employeeService.getAllSkill()
    .subscribe(
      data => {
        this.skills = data.payload;
      },
      error => {
        console.log(error);
      });
  }

  resetForms(){
    this.addFormActive = false;
    this.editFormActive = false;
    this.skillEditForm.reset();
    this.skillCreateForm.reset();
    this.currentSkillId = null;
  }

  showCreateForm(){
    this.addFormActive = true;
    this.editFormActive = false;
    this.skillEditForm.reset();
  }

  showEditForm(skillData){
    this.addFormActive = false;
    this.editFormActive = true;
    this.skillCreateForm.reset();
    this.currentSkillId = skillData.id;
    this.skillEditForm.setValue({'name': skillData.name});
  }

  createSkill(){
    let formData = this.skillCreateForm.value;
    this.employeeService.createSkill(formData).subscribe(
      response => {
        this.retrieveSkills();
        this.notify.showToast("Category Created Successfully", "Success", "success");
        this.resetForms();
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    );
  }

  updateSkill(){
    let formData = this.skillEditForm.value;
    this.employeeService.updateSkill(this.currentSkillId, formData).subscribe(
      response => {
        this.retrieveSkills();
        this.notify.showToast("Category Updated Successfully", "Success", "success");
        this.resetForms();
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    );
  }

  goBack(){
    this.navigationService.back();
  }


}
