import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { EmployeeService, NavigationService, NotificationService } from '../../../@core/utils';

import { Observable, of} from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'ngx-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.scss']
})
export class EmployeeCreateComponent implements OnInit {

  employeeForm: FormGroup;
  submitted: boolean = false;
  categories:any = [];


  private base64textString:String="";

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  paidFrequencyOptions:any = [
    { title:'Hourly', value:"hourly" },
    { title:'Daily', value:"daily" },
    { title:'Weekly', value:"weekly" },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private employeeService: EmployeeService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router,
  ) { }

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      // rrule: new FormControl(''),
      name: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
      ]),
      category_id: new FormControl('', [
        Validators.required,
      ]),
      description: new FormControl('', [
        Validators.required,
      ]),
      address: new FormControl('', [
        Validators.required,
      ]),
      base_rate: new FormControl(0, [
        Validators.required,
      ]),
      frequency: new FormControl('hourly', [
        Validators.required,
      ]),
      license_number: new FormControl('', [
        Validators.required,
      ]),
      status: new FormControl(1),
      bsb_number: new FormControl('', [
        Validators.required,
      ]),
      account_number: new FormControl('', [
        Validators.required,
      ]),
      phone_number: new FormControl('', [
        Validators.required,
      ]),
      photo: new FormControl('', [
        Validators.required,
      ]),
      fileSource: new FormControl('', [Validators.required])
    });

    this.getCategories();

  }

  getCategories(){
    this.employeeService.getAllCategory().subscribe(
      response => {
        this.categories = response.payload;
      },
      error => {
        console.log(error.message)
      }
    )
  }

  get f(){
    return this.employeeForm.controls;
  }

  onFileChange(event) {
  
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.employeeForm.patchValue({
        fileSource: file
      });
    }
  }


  createEmployee(){
    // let formValue = {
    // }

    const formData = new FormData();
    formData.append('photo', this.employeeForm.get('fileSource').value);
    formData.append('name', this.employeeForm.get('name').value);
    formData.append('email', this.employeeForm.get('email').value);
    formData.append('category_id', this.employeeForm.get('category_id').value);
    formData.append('skill', this.employeeForm.get('category_id').value);
    formData.append('description', this.employeeForm.get('description').value);
    formData.append('address', this.employeeForm.get('address').value);
    formData.append('status', this.employeeForm.get('status').value);
    formData.append('base_rate', this.employeeForm.get('base_rate').value);
    formData.append('frequency', this.employeeForm.get('frequency').value);
    formData.append('license_number', this.employeeForm.get('license_number').value);
    formData.append('bsb_number', this.employeeForm.get('bsb_number').value);
    formData.append('account_number', this.employeeForm.get('account_number').value);
    formData.append('phone_number', this.employeeForm.get('phone_number').value);
    


    // console.log(this.employeeForm.value);

    if(formData){
      this.employeeService.create(formData).subscribe(
        response => {
          console.log(response);
          this.notify.showToast("New Employee Created Successfully", "Success", "success");
          this.router.navigate(['pages/employee/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        }
      );
    }else{
      this.notify.showToast("Please Select Image", "Error", "danger");
    }


    
  }

  goBack(){
    this.NavigationService.back();
  }

}
