import { Component, OnInit } from '@angular/core';

import { NbDialogService } from '@nebular/theme';

import { EmployeeService, NavigationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'ngx-employee-listing',
  templateUrl: './employee-listing.component.html',
  styleUrls: ['./employee-listing.component.scss']
})
export class EmployeeListingComponent implements OnInit {


  allemployees:any;
  employees:any;
  currentIndex = -1;
  employeeName = '';

  employeeFilterNgModel:string = null;

  constructor(
    private dialogService: NbDialogService,
    private navigationService: NavigationService,
    private employeeService: EmployeeService,
  ) { }

  ngOnInit() {
    this.retrieveEmployees();
  }

  retrieveEmployees(): void {
    this.employeeService.getAll()
      .subscribe(
        data => {
          this.employees = data.payload;
          this.allemployees = data.payload;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveEmployees();
  }

  delete(id) {
    this.dialogService.open(DeleteConfirmationComponent)
      .onClose.subscribe(deleteValue => {
        if(deleteValue == "ok"){
          this.employeeService.delete(id)
          .subscribe (
            response => {
              console.log(response);
              this.refreshList();
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  // searchTitle(): void {
  //   this.employeeService.findByName(this.clientName)
  //     .subscribe(
  //       data => {
  //         this.clients = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  employeeNameSearch(searchValue){
    this.employees = this.allemployees.filter(item => {
      return item.name.toLowerCase().includes(searchValue.toLowerCase());
    });
  }

  goBack(){
    this.navigationService.back();
  }

}
