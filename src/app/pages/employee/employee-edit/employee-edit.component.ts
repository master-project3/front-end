import { Component, OnInit, AfterViewInit ,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { EmployeeService, NavigationService, NotificationService } from '../../../@core/utils';

@Component({
  selector: 'ngx-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.scss']
})
export class EmployeeEditComponent implements AfterViewInit {

  errors: string[] = [];
  statusValue: number = null;
  employeeId = null;
  employeeForm: FormGroup;
  submitted: boolean = false;
  categories:any = [];

  private base64textString:String="";

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  paidFrequencyOptions:any = [
    { title:'Hourly', value:"hourly" },
    { title:'Daily', value:"daily" },
    { title:'Weekly', value:"weekly" },
  ];

  currentEmployee: any = {
    name: '',
    abn:'',
    address:'',
    contact_no:'',
    url:'',
    email:'',
    status:0
  };


  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
    protected cd: ChangeDetectorRef,
    private NavigationService: NavigationService,
    private notify: NotificationService,
  ) {

    this.employeeForm = this.formBuilder.group({
      // rrule: new FormControl(''),
      name: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
      ]),
      category_id: new FormControl('', [
        Validators.required,
      ]),
      description: new FormControl('', [
        Validators.required,
      ]),
      address: new FormControl('', [
        Validators.required,
      ]),
      base_rate: new FormControl(0, [
        Validators.required,
      ]),
      frequency: new FormControl('hourly', [
        Validators.required,
      ]),
      license_number: new FormControl('', [
        Validators.required,
      ]),
      status: new FormControl(1),
      bsb_number: new FormControl('', [
        Validators.required,
      ]),
      account_number: new FormControl('', [
        Validators.required,
      ]),
      phone_number: new FormControl('', [
        Validators.required,
      ]),
      photo: new FormControl('', []),
    });

    this.employeeId = this.route.snapshot.paramMap.get('id');
    this.getCategories();
    this.getEmployee(this.employeeId);

  }

  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  getCategories(){
    this.employeeService.getAllCategory().subscribe(
      response => {
        this.categories = response.payload;
      },
      error => {
        console.log(error.message)
      }
    )
  }

  getEmployee(id): void {
    this.employeeService.get(id)
      .subscribe(
        data => {
          let employeeData = {
            name: data?.payload?.name,
            phone_number: data?.payload?.phone_number,
            category_id: data?.payload?.category_id,
            photo: '',
            account_number: data?.payload?.account_number,
            base_rate: data?.payload?.base_rate,
            address: data?.payload?.address,
            bsb_number: data?.payload?.bsb_number,
            description: data?.payload?.description,
            email: data?.payload?.email,
            frequency: data?.payload?.frequency,
            license_number: data?.payload?.license_number || '',
            status: data?.payload?.status
          };
          // this.currentEmployee = employeeData;
          this.employeeForm.setValue(employeeData)
          // this.currentClient.status = clientData.status;
          
        },
        error => {
          console.log(error);
        });
  }


  get f(){
    return this.employeeForm.controls;
  }

  onFileChange(event) {
  
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.employeeForm.patchValue({
        fileSource: file
      });
    }
  }


  updateEmployee(){
    const formData = new FormData();
    formData.append('photo', this.employeeForm.get('fileSource').value);
    formData.append('name', this.employeeForm.get('name').value);
    formData.append('email', this.employeeForm.get('email').value);
    formData.append('category_id', this.employeeForm.get('category_id').value);
    formData.append('skill', this.employeeForm.get('category_id').value);
    formData.append('description', this.employeeForm.get('description').value);
    formData.append('address', this.employeeForm.get('address').value);
    formData.append('status', this.employeeForm.get('status').value);
    formData.append('base_rate', this.employeeForm.get('base_rate').value);
    formData.append('frequency', this.employeeForm.get('frequency').value);
    formData.append('license_number', this.employeeForm.get('license_number').value);
    formData.append('bsb_number', this.employeeForm.get('bsb_number').value);
    formData.append('account_number', this.employeeForm.get('account_number').value);
    formData.append('phone_number', this.employeeForm.get('phone_number').value);

    // console.log(this.employeeForm.value);

    this.employeeService.update(this.employeeId, formData).subscribe(
      response => {
        console.log(response);
        this.notify.showToast("Employee Detail Updated Successfully", "Success", "success");
        this.router.navigate(['pages/employee/list']);
      },
      error => {
        console.log(error);
        this.notify.showToast(error.message, "Error", "danger");
      }
    );


    
  }

  goBack(){
    this.NavigationService.back();
  }

}
