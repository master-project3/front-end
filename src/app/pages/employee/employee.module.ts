import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe, TitleCasePipe } from '@angular/common';


import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbTimepickerModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { EmployeeRoutingModule } from './employee-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';

import { EmployeeComponent } from './employee.component';
import { EmployeeListingComponent } from './employee-listing/employee-listing.component';
import { EmployeeCreateComponent } from './employee-create/employee-create.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeAvailabilityComponent } from './employee-availability/employee-availability.component';
import { SkillCategoryComponent } from './skill-category/skill-category.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTimepickerModule,
    EmployeeRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
    NbFormFieldModule,
  ],
  declarations: [
    EmployeeComponent,
    EmployeeListingComponent,
    EmployeeCreateComponent,
    EmployeeEditComponent,
    EmployeeAvailabilityComponent,
    SkillCategoryComponent,
  ],
  providers:[DatePipe]
})

export class EmployeeModule { }
