import { Component, OnInit, AfterViewInit ,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { EmployeeService, NavigationService, NotificationService } from '../../../@core/utils';

import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employee-availability',
  templateUrl: './employee-availability.component.html',
  styleUrls: ['./employee-availability.component.scss']
})
export class EmployeeAvailabilityComponent implements OnInit {

  submitted: boolean = false;
  employeeId = null;
  availabilityForm: FormGroup;
  availabilities:any = [];

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private router: Router,
    protected cd: ChangeDetectorRef,
    private NavigationService: NavigationService,
    private notify: NotificationService,
    private datePipe: DatePipe,
  ) {
    
  }

  ngOnInit() {
    this.employeeId = this.route.snapshot.paramMap.get('id');

    this.availabilityForm = this.formBuilder.group({
      monday: new FormControl(false),
      tuesday: new FormControl(false),
      wednesday: new FormControl(false),
      thursday: new FormControl(false),
      friday: new FormControl(false),
      saturday: new FormControl(false),
      sunday: new FormControl(false),
      availability_for_start_date: new FormControl(),
      availability_for_end_date: new FormControl(),
      status: new FormControl(1),
    });

    this.getEmployeeAvailability();

  }
  
  getEmployeeAvailability(){
    this.employeeService.getAvailabilityByEmployee(this.employeeId).subscribe(
      response => {
        this.availabilities = response?.payload || [];
      },
      error => {
        this.notify.showToast("Unexpected Error, Please Try Again", "Error", "danger");
      }
    )
  }

  createAvailability(){
    console.log(this.availabilityForm.value);

    this.availabilityForm.value['employee_id'] = this.employeeId;
    this.availabilityForm.value['availability_for_start_date'] = this.datePipe.transform(this.availabilityForm?.value?.availability_for_start_date, 'Y-MM-dd');
    this.availabilityForm.value['availability_for_end_date'] = this.datePipe.transform(this.availabilityForm?.value?.availability_for_end_date, 'Y-MM-dd');

    // let formObject = {};
    
    // if(this.employeeId){
    //   formObject['employee_id'] = this.employeeId;
    //   formObject['status'] = this.availabilityForm?.value?.status;
    //   formObject['availability_for_start_date'] = this.datePipe.transform(this.availabilityForm?.value?.availability_for_start_date, 'Y-MM-dd');
    //   formObject['availability_for_end_date'] = this.datePipe.transform(this.availabilityForm?.value?.availability_for_end_date, 'Y-MM-dd');
    //   formObject['employee_hour'] = {};
    //   if(this.availabilityForm?.value?.mon_start && this.availabilityForm?.value?.mon_end){
    //     formObject['employee_hour']['monday'] = {};
    //     formObject['employee_hour']['monday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.mon_start, 'HH:mm:ss');
    //     formObject['employee_hour']['monday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.mon_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.tue_start && this.availabilityForm?.value?.tue_end){
    //     formObject['employee_hour']['tuesday'] = {};
    //     formObject['employee_hour']['tuesday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.tue_start, 'HH:mm:ss');
    //     formObject['employee_hour']['tuesday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.tue_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.wed_start && this.availabilityForm?.value?.wed_end){
    //     formObject['employee_hour']['wednesday'] = {};
    //     formObject['employee_hour']['wednesday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.wed_start, 'HH:mm:ss');
    //     formObject['employee_hour']['wednesday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.wed_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.thur_start && this.availabilityForm?.value?.thur_end){
    //     formObject['employee_hour']['thursday'] = {};
    //     formObject['employee_hour']['thursday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.thur_start, 'HH:mm:ss');
    //     formObject['employee_hour']['thursday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.thur_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.fri_start && this.availabilityForm?.value?.fri_end){
    //     formObject['employee_hour']['friday'] = {};
    //     formObject['employee_hour']['friday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.fri_start, 'HH:mm:ss');
    //     formObject['employee_hour']['friday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.fri_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.sat_start && this.availabilityForm?.value?.sat_end){
    //     formObject['employee_hour']['saturday'] = {};
    //     formObject['employee_hour']['saturday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.sat_start, 'HH:mm:ss');
    //     formObject['employee_hour']['saturday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.sat_end, 'HH:mm:ss');
    //   }
    //   if(this.availabilityForm?.value?.sun_start && this.availabilityForm?.value?.sun_end){
    //     formObject['employee_hour']['sunday'] = {};
    //     formObject['employee_hour']['sunday']['start_time'] = this.datePipe.transform(this.availabilityForm?.value?.sun_start, 'HH:mm:ss');
    //     formObject['employee_hour']['sunday']['end_time'] = this.datePipe.transform(this.availabilityForm?.value?.sun_end, 'HH:mm:ss');
    //   }

      let formObject = this.availabilityForm.value;

      this.employeeService.createAvailability(formObject).subscribe(
        response => {
          console.log(response);
          this.notify.showToast("Employee Availability Created Successfully", "Success", "success");
          this.router.navigate(['pages/employee/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        }
      );
    
  }

  goBack(){
    this.NavigationService.back();
  }

}
