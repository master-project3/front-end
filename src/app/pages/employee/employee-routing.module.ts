import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { EmployeeComponent } from "./employee.component";
import { EmployeeListingComponent } from "./employee-listing/employee-listing.component";
import { EmployeeCreateComponent } from "./employee-create/employee-create.component";
import { EmployeeEditComponent } from "./employee-edit/employee-edit.component";
import { SkillCategoryComponent } from "./skill-category/skill-category.component";
import { EmployeeAvailabilityComponent } from "./employee-availability/employee-availability.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: EmployeeComponent,
    children: [
      {
        path: "list",
        component: EmployeeListingComponent,
      },
      {
        path: "create",
        component: EmployeeCreateComponent,
      },
      {
        path: "edit/:id",
        component: EmployeeEditComponent,
      },
      {
        path: ":id/availability",
        component: EmployeeAvailabilityComponent
      },
      {
        path: "skill-category",
        component: SkillCategoryComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}

export const routedComponents = [
    EmployeeComponent,
    EmployeeListingComponent,
    EmployeeCreateComponent,
    EmployeeEditComponent,
    EmployeeAvailabilityComponent,
    SkillCategoryComponent
  ];