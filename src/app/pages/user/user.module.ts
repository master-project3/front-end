import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';


import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbTimepickerModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { UserRoutingModule } from './user-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';

import { UserComponent } from './user.component';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserEditComponent } from './user-edit/user-edit.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTimepickerModule,
    UserRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
    NbFormFieldModule,
  ],
  declarations: [
    UserComponent,
    UserListingComponent,
    UserCreateComponent,
    UserEditComponent,
  ],
  providers:[DatePipe]
})

export class UserModule { }
