import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { UserComponent } from "./user.component";
import { UserListingComponent } from "./user-listing/user-listing.component";
import { UserCreateComponent } from "./user-create/user-create.component";
import { UserEditComponent } from "./user-edit/user-edit.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: UserComponent,
    children: [
      {
        path: "list",
        component: UserListingComponent,
      },
      {
        path: "create",
        component: UserCreateComponent,
      },
      {
        path: "edit/:id",
        component: UserEditComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}

export const routedComponents = [
    UserComponent,
    UserListingComponent,
    UserCreateComponent,
    UserEditComponent,
  ];