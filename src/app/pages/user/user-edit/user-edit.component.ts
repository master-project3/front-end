import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminUserService, CurrentUserService, NavigationService, NotificationService, RoleService } from '../../../@core/utils';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  userForm: FormGroup;
  submitted: boolean = false;
  userId = null;

  roles:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private userService: AdminUserService,
    private roleService: RoleService,
    private notify: NotificationService,
    private router: Router,
    private route: ActivatedRoute,
    private cus: CurrentUserService,
  ) { }

  ngOnInit() {



    this.userForm = this.formBuilder.group({

      name: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
      ]),
      address: new FormControl(''),
      role_id: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required,
      ]),
      cpassword: new FormControl('',[
        Validators.required
      ])
    });

    this.userId = this.route.snapshot.paramMap.get('id');

    let role = this.cus.getRole();
    if(role!="admin" && this.userId != this.cus.getUserData()?.id){
      this.notify.showToast("Unauthorized Access", "Error", "danger");
      this.router.navigate(['/']);
    }

    this.getAllRoles();
    this.getUser(this.userId);
  }

  getAllRoles(){
    this.roleService.getAll().subscribe(
      response => {
        this.roles = response?.payload;
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    )
  }

  getUser(id){
    this.userService.get(id).subscribe(
      response => {
        console.log(response.payload);

        let data = {
          name: response?.payload?.name,
          email: response?.payload?.email,
          address: response?.payload?.address,
          role_id: response?.payload?.role_id,
          password:"",
          cpassword:""

        };

        this.userForm.setValue(data);

      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
        this.NavigationService.back();
      }
    )
  }

  saveUser(){

    console.log(this.userForm.value);

    let formData = this.userForm.value;

    if(formData.password == formData.cpassword){
      delete formData.cpassword;
      this.userService.update(this.userId, formData).subscribe(
        response => {
          console.log(response);
          this.notify.showToast("User Detail Updated Successfully", "Success", "success");
          this.router.navigate(['pages/user/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        }
      );
    }else{
      this.notify.showToast("Password and Password Confirmation doesn't match.", "Error", "danger");
    }
    
  }

  goBack(){
    this.NavigationService.back();
  }


  // Password Toggle
  showPassword = false;
  showCPassword = false;


  getInputType(inputId) {
    if(inputId == 'cp'){
      if (this.showCPassword) {
        return 'text';
      }
      return 'password';
    }else{
      if (this.showPassword) {
        return 'text';
      }
      return 'password';
    }

  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  toggleShowCPassword() {
    this.showCPassword = !this.showCPassword;
  }

}
