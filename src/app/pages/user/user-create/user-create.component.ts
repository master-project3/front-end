import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AdminUserService, NavigationService, NotificationService, RoleService } from '../../../@core/utils';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  userForm: FormGroup;
  submitted: boolean = false;

  roles:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private userService: AdminUserService,
    private roleService: RoleService,
    private notify: NotificationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({

      name: new FormControl('', [
        Validators.required,
      ]),
      email: new FormControl('', [
        Validators.required,
      ]),
      address: new FormControl(''),
      role_id: new FormControl('', [
        Validators.required,
      ]),
      password: new FormControl('', [
        Validators.required,
      ]),
      cpassword: new FormControl('',[
        Validators.required
      ])
    });
    this.getAllRoles();
  }

  getAllRoles(){
    this.roleService.getAll().subscribe(
      response => {
        this.roles = response?.payload;
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    )
  }

  saveUser(){

    console.log(this.userForm.value);

    let formData = this.userForm.value;

    if(formData.password == formData.cpassword){
      delete formData.cpassword;
      this.userService.create(formData).subscribe(
        response => {
          console.log(response);
          this.notify.showToast("New user Created Successfully", "Success", "success");
          this.router.navigate(['pages/user/list']);
        },
        error => {
          console.log(error);
          this.notify.showToast(error.message, "Error", "danger");
        }
      );
    }else{
      this.notify.showToast("Password and Password Confirmation doesn't match.", "Error", "danger");
    }
    
  }

  goBack(){
    this.NavigationService.back();
  }


  // Password Toggle
  showPassword = false;
  showCPassword = false;


  getInputType(inputId) {
    if(inputId == 'cp'){
      if (this.showCPassword) {
        return 'text';
      }
      return 'password';
    }else{
      if (this.showPassword) {
        return 'text';
      }
      return 'password';
    }

  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  toggleShowCPassword() {
    this.showCPassword = !this.showCPassword;
  }

}
