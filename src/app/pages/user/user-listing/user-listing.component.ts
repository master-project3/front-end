import { Component, OnInit } from '@angular/core';
import { AdminUserService, NavigationService, NotificationService } from '../../../@core/utils';

import { NbDialogService } from '@nebular/theme';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit {

  allusers:any;
  users:any;

  userFilterNgModel:string = null;

  constructor(
    private dialogService: NbDialogService,
    private navigationService: NavigationService,
    private notify: NotificationService,
    private userService: AdminUserService,
  ) { }

  ngOnInit() {
    this.retrieveUsers();
  }

  retrieveUsers(): void {
    this.userService.getAll()
      .subscribe(
        data => {
          this.users = data.payload;
          this.allusers = data.payload;
        },
        error => {
          console.log(error);
        });
  }

  delete(id) {
    this.dialogService.open(DeleteConfirmationComponent)
      .onClose.subscribe(deleteValue => {
        if(deleteValue == "ok"){
          this.userService.delete(id)
          .subscribe (
            response => {
              console.log(response);
              this.refreshList();
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  refreshList(): void {
    this.retrieveUsers();
  }

  userNameSearch(searchValue){
    this.users = this.allusers.filter(item => {
      return item.name.toLowerCase().includes(searchValue.toLowerCase());
    });
  }

  goBack(){
    this.navigationService.back();
  }
}
