import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';


import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
  NbFormFieldModule,
  NbTimepickerModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { TimeSheetRoutingModule } from './time-sheet-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';

import { TimeSheetComponent } from './time-sheet.component';
import { TimeSheetListingComponent } from './time-sheet-listing/time-sheet-listing.component';
import { TimeSheetCreateComponent } from './time-sheet-create/time-sheet-create.component';
import { TimeSheetEditComponent } from './time-sheet-edit/time-sheet-edit.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTimepickerModule,
    TimeSheetRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
    NbFormFieldModule,
  ],
  declarations: [
    TimeSheetComponent,
    TimeSheetListingComponent,
    TimeSheetCreateComponent,
    TimeSheetEditComponent,
  ],
  providers:[DatePipe]
})
export class TimeSheetModule { }
