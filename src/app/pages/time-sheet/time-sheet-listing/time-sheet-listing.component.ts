import { Component, OnInit } from '@angular/core';

import { NbDialogService } from '@nebular/theme';

import { TimeSheetService, NavigationService, EmployeeService, NotificationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-time-sheet-listing',
  templateUrl: './time-sheet-listing.component.html',
  styleUrls: ['./time-sheet-listing.component.scss']
})
export class TimeSheetListingComponent implements OnInit {

  allTimeSheets:any;
  timeSheets:any = [];

  employeeFilterNgModel = null;
  employees = [];

  constructor(
    private dialogService: NbDialogService,
    private navigationService: NavigationService,
    private timeSheetService: TimeSheetService,
    private employeeService: EmployeeService,
    private notify: NotificationService,
  ) { }

  ngOnInit() {
    this.getAllEmployees();
  }

  retrieveTimeSheets(employee_id){
    this.timeSheetService.getByEmployee(employee_id)
    .subscribe(
      data => {
        this.timeSheets = data.payload;
        this.allTimeSheets = data.payload;
        if(this.allTimeSheets?.length < 1){
          this.notify.showToast("No timesheet have been filled for selected employee", "Warning", "warning");
        }
      },
      error => {
        console.log(error);
      });
  }

  // refreshList(): void {
  //   this.retrieveTimeSheets();
  // }

  delete(id) {
    this.dialogService.open(DeleteConfirmationComponent)
      .onClose.subscribe(deleteValue => {
        if(deleteValue == "ok"){
          this.timeSheetService.delete(id)
          .subscribe (
            response => {
              console.log(response);
              // this.refreshList();
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  // searchTitle(): void {
  //   this.employeeService.findByName(this.clientName)
  //     .subscribe(
  //       data => {
  //         this.clients = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  getAllEmployees(){
    this.employeeService.getAll().subscribe(
      response => {
        console.log(response.payload);
        this.employees = response.payload;
      },
      error => {
        console.log(error);
      }
    );
  }

  filerChanged(){
    if(this.employeeFilterNgModel){
      let employee_id = this.employeeFilterNgModel;
      console.log(this.employeeFilterNgModel);
      this.retrieveTimeSheets(employee_id);
    }else{
      this.employeeFilterNgModel = null;
      this.timeSheets = [];

    }
  }

  goBack(){
    this.navigationService.back();
  }



  /*
  *
  * Time Difference calculation logic for
  * employee timesheet listing
  * 
  * */

  public deltas: string[] = [];

  calculateDiff(endDate, startDate){

		var fromMs = Date.parse( startDate );
		var toMs = Date.parse( endDate );

		// Ensure that we have a valid date-range to work with.
		if ( isNaN( fromMs ) || isNaN( toMs ) || ( fromMs > toMs ) ) {

			console.group( "Invalid date range - no calculations to perform." );
			console.log( "From:", fromMs );
			console.log( "To:", toMs );
			console.groupEnd();
			return 'Invalid Date';

		}

		var deltaSeconds = ( ( toMs - fromMs ) / 1000 );

		// this.deltas = [
		// 	this.format( this.calculateHoursMinutesSeconds( deltaSeconds ) ),

		// ];

		// Strip out any deltas that start with "0". These won't add any additional
		// insight above and beyond the previous delta calculations.
		// --
		// NOTE: Always using the first value, even if "0 Seconds".
		// this.deltas = this.deltas.filter(
		// 	( value, index ) => {

		// 		return( ! index || ! value.startsWith( "0" ) );

		// 	}
		// );

    return this.format( this.calculateHoursMinutesSeconds( deltaSeconds ) );

  }


  	// ---
	// PRIVATE METHODS.
	// ---

	// I calculate the delta breakdown using Day as the largest unit.
	private calculateDaysHoursMinutesSeconds( delta: number ) : number[] {

		var days = Math.floor( delta / 60 / 60 / 24 );
		var remainder = ( delta - ( days * 60 * 60 * 24 ) );

		return( [ days, ...this.calculateHoursMinutesSeconds( remainder ) ] );

	}


	// I calculate the delta breakdown using Hour as the largest unit.
	private calculateHoursMinutesSeconds( delta: number ) : number[] {

		var hours = Math.floor( delta / 60 / 60 );
		var remainder = ( delta - ( hours * 60 * 60 ) );

		return( [ hours, ...this.calculateMinutesSeconds( remainder ) ] );

	}


	// I calculate the delta breakdown using Minute as the largest unit.
	private calculateMinutesSeconds( delta: number ) : number[] {

		var minutes = Math.floor( delta / 60 );
		var remainder = ( delta - ( minutes * 60 ) );

		return( [ minutes, ...this.calculateSeconds( remainder ) ] );

	}


	// I calculate the delta breakdown using Second as the largest unit.
	private calculateSeconds( delta: number ) : number[] {

		return( [ delta ] );

	}


	// I calculate the delta breakdown using Week as the largest unit.
	private calculateWeeksDaysHoursMinutesSeconds( delta: number ) : number[] {

		var weeks = Math.floor( delta / 60 / 60 / 24 / 7 );
		var remainder = ( delta - ( weeks * 60 * 60 * 24 * 7 ) );

		return( [ weeks, ...this.calculateDaysHoursMinutesSeconds( remainder ) ] );

	}


	// I format the set of calculated delta-values as a human readable string.
	private format( values: number[] ) : string {

		// var units: string[] = [ "Weeks", "Days", "Hours", "Minutes", "Seconds" ];
    var units: string[] = [ "Weeks", "Days", "Hours", "Minutes", "Seconds" ];
		var parts: string[] = [];

		// Since the values are calculated largest to smallest, let's iterate over them
		// backwards so that we know which values line up with which units.
    // console.log(values);
		for ( var value of values.slice().reverse() ) {
      if(value == 0){
        units.pop();
      }else{
        parts.unshift( value.toLocaleString() + " " + units.pop() );
      }
      

		}

		return( parts.join( ", " ) );

	}

}
