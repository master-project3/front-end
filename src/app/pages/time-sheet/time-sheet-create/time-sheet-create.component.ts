import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { EmployeeService, JobService, TimeSheetService, NavigationService, NotificationService } from '../../../@core/utils';

import { Observable, of} from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-time-sheet-create',
  templateUrl: './time-sheet-create.component.html',
  styleUrls: ['./time-sheet-create.component.scss']
})
export class TimeSheetCreateComponent implements OnInit {

  timeSheetForm: FormGroup;
  submitted: boolean = false;
  isSpecialRateChecked: boolean = false;

  employees:any = [];
  jobs:any = [];

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  SpecialShifts:any = [
    { title:'Yes', value:1 },
    { title:'No', value:0 }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private employeeService: EmployeeService,
    private jobService: JobService,
    private timeSheetService: TimeSheetService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router,
  ) { }

  disabledValue:boolean = true;

  ngOnInit() {
    this.timeSheetForm = this.formBuilder.group({
      employee_id: new FormControl('', [
        Validators.required,
      ]),
      job_id: new FormControl('', [
        Validators.required,
      ]),
      start_date_time: new FormControl('', [
        Validators.required,
      ]),
      end_date_time: new FormControl('', [
        Validators.required,
      ]),
      is_special_shift: new FormControl(0, [
        Validators.required,
      ]),
      special_rate: new FormControl(0),
    });

    this.getEmployees();

    this.timeSheetForm.get("is_special_shift").valueChanges.subscribe(isSpecialShift => {
      if(isSpecialShift == 1){
        this.timeSheetForm.controls['special_rate'].setValidators([Validators.required]);
        this.disabledValue = false;
      } else {
        this.timeSheetForm.controls['special_rate'].clearValidators();
        this.timeSheetForm.controls['special_rate'].setValue('null');
        this.disabledValue = true;
      }
      this.timeSheetForm.controls['special_rate'].updateValueAndValidity();
   });
  }

  employeeSelectChange(){
    let changedEmployeeId = this.timeSheetForm.controls['employee_id'].value;
    this.jobService.getJobsbyEmployeeId(changedEmployeeId).subscribe(
      response => {
        console.log(response);
        this.jobs = response?.payload;
        if(this.jobs?.length < 1){
          this.notify.showToast("No Jobs assigned for the selected employee", "Error", "danger");
        }
      }
    );
  }


  createTimeSheet(){

    let formValue = this.timeSheetForm.value;
    formValue.start_date_time = this.datePipe.transform(this.timeSheetForm.value.start_date_time, 'Y-MM-dd HH:mm:ss');
    formValue.end_date_time = this.datePipe.transform(this.timeSheetForm.value.end_date_time, 'Y-MM-dd HH:mm:ss')

    console.log(this.timeSheetForm.value);
    this.submitted = true;
    this.timeSheetService.create(formValue).subscribe(
      response => {
        console.log(response);
        this.notify.showToast("New Timesheet Created Successfully", "Success", "success");
        this.router.navigate(['pages/time-sheet/list']);
      },
      error => {
        console.log(error);
        this.notify.showToast(error.message, "Error", "danger");
        this.submitted = false;
      }
    );

  }

  getEmployees(){
    this.employeeService.getAll().subscribe(
      response => {
        console.log(response);
        this.employees = response?.payload;
      },
      error => {
        console.log(error);
      }
    );
  }

  goBack(){
    this.NavigationService.back();
  }

}
