import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { TimeSheetComponent } from "./time-sheet.component";
import { TimeSheetListingComponent } from "./time-sheet-listing/time-sheet-listing.component";
import { TimeSheetCreateComponent } from "./time-sheet-create/time-sheet-create.component";
import { TimeSheetEditComponent } from "./time-sheet-edit/time-sheet-edit.component";
// import { EmployeeEditComponent } from "./employee-edit/employee-edit.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: TimeSheetComponent,
    children: [
      {
        path: "list",
        component: TimeSheetListingComponent,
      },
      {
        path: "create",
        component: TimeSheetCreateComponent,
      },
      {
        path: "edit/:id",
        component: TimeSheetEditComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeSheetRoutingModule {}

export const routedComponents = [
    TimeSheetComponent,
    TimeSheetListingComponent,
    TimeSheetCreateComponent,
    TimeSheetEditComponent,
  ];