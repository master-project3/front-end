import { Component, OnInit, AfterViewInit ,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { EmployeeService, JobService, TimeSheetService, NavigationService, NotificationService } from '../../../@core/utils';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-time-sheet-edit',
  templateUrl: './time-sheet-edit.component.html',
  styleUrls: ['./time-sheet-edit.component.scss']
})
export class TimeSheetEditComponent implements AfterViewInit {

  timeSheetForm: FormGroup;
  timesheetId = null;
  submitted: boolean = false;
  isSpecialRateChecked: boolean = false;
  disabledValue:boolean = true;
  timeSheetData:any = {};

  disableEmployee:boolean = true;

  employees:any = [];
  jobs:any = [];

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  SpecialShifts:any = [
    { title:'Yes', value:1 },
    { title:'No', value:0 }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private employeeService: EmployeeService,
    private jobService: JobService,
    private timeSheetService: TimeSheetService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute,
    protected cd: ChangeDetectorRef,
  ) {

    this.timeSheetForm = this.formBuilder.group({
      employee_id: new FormControl('', [
        Validators.required,
      ]),
      job_id: new FormControl('', [
        Validators.required,
      ]),
      start_date_time: new FormControl('', [
        Validators.required,
      ]),
      end_date_time: new FormControl('', [
        Validators.required,
      ]),
      is_special_shift: new FormControl(0, [
        Validators.required,
      ]),
      special_rate: new FormControl(0),
    });

    this.getEmployees();

    this.timeSheetForm.get("is_special_shift").valueChanges.subscribe(isSpecialShift => {
      if(isSpecialShift == 1){
        this.timeSheetForm.controls['special_rate'].setValidators([Validators.required]);
        this.disabledValue = false;
      } else {
        this.timeSheetForm.controls['special_rate'].clearValidators();
        this.timeSheetForm.controls['special_rate'].setValue('null');
        this.disabledValue = true;
      }
      this.timeSheetForm.controls['special_rate'].updateValueAndValidity();
   });

   this.timesheetId = this.route.snapshot.paramMap.get('id');
   this.getTimeSheet(this.timesheetId);

  }


  ngAfterViewInit(): void {
    this.cd.detectChanges();
  }

  getTimeSheet(id){
    this.timeSheetService.get(id).subscribe(
      response =>{
        this.timeSheetData = response?.payload;
        this.loadJobsByEmployee(this.timeSheetData.employee_id);

        let timeSheetObj = {
          'employee_id': this.timeSheetData.employee_id ,
          'job_id': this.timeSheetData.job_id,
          'start_date_time': new Date(this.timeSheetData.start_date_time),
          'end_date_time': new Date(this.timeSheetData.end_date_time),
          'is_special_shift': this.timeSheetData.is_special_shift,
          'special_rate': (this.timeSheetData.is_special_shift == 1 ? this.timeSheetData.special_rate : null ),
        }

        this.timeSheetForm.setValue(timeSheetObj);

      },
      error => {
        this.notify.showToast(error?.message, "Error", "danger");
      }
    )
  }


  getEmployees(){
    this.employeeService.getAll().subscribe(
      response => {
        console.log(response);
        this.employees = response?.payload;
      },
      error => {
        console.log(error);
      }
    );
  }

  employeeSelectChange(){
    let changedEmployeeId = this.timeSheetForm.controls['employee_id'].value;
    this.loadJobsByEmployee(changedEmployeeId);
  }

  loadJobsByEmployee(employeeId){
    this.jobService.getJobsbyEmployeeId(employeeId).subscribe(
      response => {
        console.log(response);
        this.jobs = response?.payload;
        if(this.jobs?.length < 1){
          this.notify.showToast("No Jobs assigned for the selected employee", "Error", "danger");
        }
      }
    );
  }

  updateTimeSheet(){
    let formValue = this.timeSheetForm.value;
    formValue.start_date_time = this.datePipe.transform(this.timeSheetForm.value.start_date_time, 'Y-MM-dd HH:mm:ss');
    formValue.end_date_time = this.datePipe.transform(this.timeSheetForm.value.end_date_time, 'Y-MM-dd HH:mm:ss')

    this.submitted = true;
    this.timeSheetService.update(this.timesheetId, formValue).subscribe(
      response => {
        this.notify.showToast("Timesheet Updated Successfully", "Success", "success");
        this.router.navigate(['pages/time-sheet/list']);
      },
      error => {
        console.log(error);
        this.notify.showToast(error.message, "Error", "danger");
        this.submitted = false;
      }
    );
  }

  goBack(){
    this.NavigationService.back();
  }

}
