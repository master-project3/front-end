import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { JobComponent } from "./job.component";
import { JobListingComponent } from "./job-listing/job-listing.component";
import { JobCreateComponent } from "./job-create/job-create.component";
import { JobEditComponent } from "./job-edit/job-edit.component";
import { JobAssignComponent } from "./job-assign/job-assign.component";

// import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
// import { DatepickerComponent } from './datepicker/datepicker.component';
// import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [
  {
    path: "",
    component: JobComponent,
    children: [
      {
        path: "list",
        component: JobListingComponent,
      },
      {
        path: "create",
        component: JobCreateComponent,
      },
      {
        path: "edit/:id",
        component: JobEditComponent,
      },
      {
        path: ":id/assign",
        component: JobAssignComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobRoutingModule {}

export const routedComponents = [
  JobComponent,
  JobListingComponent,
  JobCreateComponent,
  JobEditComponent,
  JobAssignComponent,
];
