import { Component, OnInit, AfterViewInit ,ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { ClientService, JobService, NavigationService, NotificationService } from '../../../@core/utils';

@Component({
  selector: 'ngx-job-edit',
  templateUrl: './job-edit.component.html',
  styleUrls: ['./job-edit.component.scss']
})
export class JobEditComponent implements AfterViewInit {

  jobId = null;

  jobForm: FormGroup;
  submitted: boolean = false;
  isRecurringCheck: string = 'recurring-disabled';
  jobDetail:any;
  categories:any = [];

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  paidFrequencyOptions:any = [
    { title:'Weekly', value:"weekly" },
    { title:'Fortnightly', value:"fortnightly" },
    { title:'Monthly', value:"monthly" },
  ];

  recurringOptions:any = [
    { title:'Yes', value:1 },
    { title:'No', value:0 }
  ];

  //For Client Selection Autocomplete
  options:any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    protected cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private clientService: ClientService,
    private jobService: JobService,
    private notify: NotificationService,
    private datePipe: DatePipe
  ) {
    this.jobForm = this.formBuilder.group({
      rrule: new FormControl(''),
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      start_date: new FormControl(new Date()),
      end_date: new FormControl(new Date()),
      location: new FormControl(''),
      status: new FormControl(''),
      no_of_employee: new FormControl(''),
      paid_frequency: new FormControl(''),
      base_rate: new FormControl(''),
      category_id: new FormControl(''),
      client_id: new FormControl(''),
      note: new FormControl(''),
      is_recurring: new FormControl(''),
      description: new FormControl(''),
    });

    this.jobId = this.route.snapshot.paramMap.get('id');
    this.getClients();
    this.getCategories();
    this.getJobDetail(this.jobId);

    this.jobForm.valueChanges.subscribe(() => {
      const rRuleFormValue = this.jobForm.value.rrule;

      if(this.jobForm.value.is_recurring == 1){
        this.isRecurringCheck = 'recurring-enabled';
      }else{
        this.isRecurringCheck = 'recurring-disabled';
      }
      
      // Get the rrule object.
      // This is an instance of RRule. Refer to https://github.com/jakubroztocil/rrule on how to use it
      // console.log(rRuleFormValue.rRule.toString());
      
      // Optional - Raw value of the ngxRrule used internally
      // console.log(rRuleFormValue.raw);
    });

  }

  getCategories(){
    this.jobService.getAllCategory().subscribe(
      response => {
        this.categories = response.payload;
      },
      error => {
        console.log(error.message)
      }
    )
  }

  ngAfterViewInit(): void {
    // this.getClient(this.clientId);
    this.cd.detectChanges();
  }

  getClients(){
    this.clientService.getAll().subscribe(
      response => {
        this.options = response.payload;
      },
      error => {
        console.log("Get Client List Error")
      }
    )
  }

  getJobDetail(jobId){
    this.jobService.get(jobId).subscribe(
      response => {
        console.log(response);
        this.jobDetail = response;
        let formValue = {
          "title" : response.title,
          "start_date" : response.start_date,
          "end_date" : response.end_date,
          "location" : response.location,
          "status" : response.status,
          "rrule" : response.rrule,
          "no_of_employee" : response.no_of_employee,
          "paid_frequency" : response.paid_frequency,
          "base_rate" : response.base_rate,
          "client_id" : response.client_id,
          "category_id" : response.category_id,
          "note" : response.note,
          "is_recurring" : response.is_recurring,
          "description" : response.description,
        }

          this.jobForm.setValue(formValue);    
        
      },
      error => {
        this.notify.showToast(error.message, "Error", "danger");
      }
    )

  }


  updateJobRequest(){
    let formValue = {
      "title" : this.jobForm.value.title,
      "start_date" : this.datePipe.transform(this.jobForm.value.start_date, 'Y-MM-dd HH:mm:ss'),
      "end_date" : this.datePipe.transform(this.jobForm.value.end_date, 'Y-MM-dd HH:mm:ss'),
      "location" : this.jobForm.value.location,
      "status" : this.jobForm.value.status,
      "rrule" : this.jobForm.value.rrule?.rRule?.toString(),
      "no_of_employee" :this.jobForm.value.no_of_employee,
      "paid_frequency" : this.jobForm.value.paid_frequency,
      "base_rate" : this.jobForm.value.base_rate,
      "client_id" : this.jobForm.value.client_id,
      "category_id" : this.jobForm.value.category_id,
      "note" : this.jobForm.value.note,
      "is_recurring" : this.jobForm.value.is_recurring,
      "description" : this.jobForm.value.description,
      "is_full_day_event" : 0,
      "is_accepted": 0,
      "recurring_type": this.jobDetail?.formatted_rrule?.recurring_type
    }

    this.jobService.update(this.jobId, formValue).subscribe(
      response => {
        console.log(response);
        this.notify.showToast("Job Request Updated Successfully", "Success", "success");
        this.router.navigate(['pages/jobs/list']);
      },
      error => {
        console.log(error);
        this.notify.showToast(error.message, "Error", "danger");
      }
    );
    console.log(this.jobForm.value);
  }

  goBack(){
    this.NavigationService.back();
  }

}
