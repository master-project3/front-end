import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NbDialogService, NbWindowRef, NbWindowService } from '@nebular/theme';

import { CurrentUserService, JobService, NavigationService } from '../../../@core/utils';
import { DeleteConfirmationComponent } from '../../modal-overlays/delete-confirmation/delete-confirmation.component';

@Component({
  selector: 'app-job-listing',
  templateUrl: './job-listing.component.html',
  styleUrls: ['./job-listing.component.scss']
})
export class JobListingComponent implements OnInit {

  jobs:any;
  currentJob = null;
  currentIndex = -1;
  JobTitle = '';
  jobFilterNgModel = "all";
  allJobs:any = {};
  loading:boolean = false;

  windowReference:any;
  @ViewChild('windowTemplate') windowTemplate: TemplateRef<any>;
  current_role: any;

  constructor(
    private dialogService: NbDialogService,
    private NavigationService: NavigationService,
    private windowService: NbWindowService,
    private jobService: JobService,
    private cus: CurrentUserService,
    ) {}

  ngOnInit(): void {
    this.retrievecJobs();
    this.current_role = this.cus.getRole();
  }

  retrievecJobs(): void {
    this.jobService.getAll()
      .subscribe(
        data => {
          this.allJobs = data.payload;
          this.jobs = data.payload;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrievecJobs();
    this.currentJob = null;
    this.currentIndex = -1;
  }

  setActiveJob(tutorial, index): void {
    this.currentJob = tutorial;
    this.currentIndex = index;
  }

  delete(id) {
    this.dialogService.open(DeleteConfirmationComponent)
      .onClose.subscribe(deleteValue => {
        if(deleteValue == "ok"){
          this.jobService.delete(id)
          .subscribe (
            response => {
              console.log(response);
              this.refreshList();
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  viewDetail(jobDetail){
    this.windowReference = this.windowService.open(
      this.windowTemplate,
      { title:'Job Detail', context: jobDetail}
    );
  }

  approveJob(jobId){
    this.loading = true;
    // alert('Job Id: '+ jobId );
    let approveData = {
      "change_job_acceptance": 1,
      "is_accepted": 1
    };
    this.jobService.update(jobId, approveData).subscribe(
      response => {
        console.log("Success");
        this.windowReference.close();
        this.refreshList();
        this.loading = false;
      },
      error => {
        console.log("Error");
        this.loading = false

      }
    )
  }

  // removeAlljobs(): void {
  //   this.jobService.deleteAll()
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         this.retrievecJobs();
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }

  searchTitle(): void {
    this.jobService.findByName(this.JobTitle)
      .subscribe(
        data => {
          this.jobs = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  filerChanged(){
    console.log(this.jobFilterNgModel);
    if(this.jobFilterNgModel == "approved"){
      this.jobs = this.allJobs.filter(job => job.is_accepted == 1);
    }else if(this.jobFilterNgModel == "pending"){
      this.jobs = this.allJobs.filter(job => job.is_accepted == 0);
    } else {
      this.jobs = this.allJobs;
    }
  }

  goBack(){
    this.NavigationService.back();
  }

}
