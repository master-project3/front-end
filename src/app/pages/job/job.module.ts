import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, CurrencyPipe, TitleCasePipe } from '@angular/common';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbAutocompleteModule,
  NbWindowModule,
  NbTooltipModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { JobRoutingModule } from './job-routing.module';
import { FormsModule as ngFormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxRruleModule } from 'ngx-rrule';

import { JobComponent } from './job.component';
import { JobListingComponent } from './job-listing/job-listing.component';
import { JobCreateComponent } from './job-create/job-create.component';
import { JobEditComponent } from './job-edit/job-edit.component';
import { JobAssignComponent } from './job-assign/job-assign.component';


@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbSelectModule,
    NbRadioModule,
    NbDatepickerModule,
    JobRoutingModule,
    ngFormsModule,
    ReactiveFormsModule,
    NgxRruleModule,
    NbAutocompleteModule,
    NbWindowModule.forChild(),
    NbTooltipModule,
  ],
  declarations: [
    JobComponent,
    JobListingComponent,
    JobCreateComponent,
    JobEditComponent,
    JobAssignComponent,
  ],
  providers:[
    DatePipe,
    CurrencyPipe,
    TitleCasePipe,
  ]
})
export class JobModule { }
