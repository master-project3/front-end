import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { ClientService, JobService, NavigationService, NotificationService } from '../../../@core/utils';

import { Observable, of} from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-job-create',
  templateUrl: './job-create.component.html',
  styleUrls: ['./job-create.component.scss']
})
export class JobCreateComponent implements OnInit {

  jobForm: FormGroup;
  submitted: boolean = false;
  isRecurringCheck: string = 'recurring-disabled';

  statusOptions:any = [
    { title:'Active', value:1 },
    { title:'Inactive', value:0 }
  ];

  paidFrequencyOptions:any = [
    { title:'Weekly', value:"weekly" },
    { title:'Fortnightly', value:"fortnightly" },
    { title:'Monthly', value:"monthly" },
  ];

  recurringOptions:any = [
    { title:'Yes', value:1 },
    { title:'No', value:0 }
  ];

  categories:any = [];

  //For Client Selection Autocomplete
  options:any = [];

  constructor(
    private formBuilder: FormBuilder,
    private NavigationService:NavigationService,
    private clientService: ClientService,
    private jobService: JobService,
    private notify: NotificationService,
    private datePipe: DatePipe,
    private router: Router
    ) { }
  
  ngOnInit() {
    this.jobForm = this.formBuilder.group({
      rrule: new FormControl(''),
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      start_date: new FormControl(new Date()),
      end_date: new FormControl(new Date()),
      location: new FormControl(''),
      status: new FormControl(1),
      no_of_employee: new FormControl(''),
      paid_frequency: new FormControl(''),
      base_rate: new FormControl(''),
      client_id: new FormControl(''),
      category_id: new FormControl(''),
      note: new FormControl(''),
      is_recurring: new FormControl(0),
      description: new FormControl(''),
    });

    this.getClients();
    this.getCategories();

    // For Recurrance Rule value change event listner
    this.jobForm.valueChanges.subscribe(() => {
      const rRuleFormValue = this.jobForm.value.rrule;

      if(this.jobForm.value.is_recurring == 1){
        this.isRecurringCheck = 'recurring-enabled';
      }else{
        this.isRecurringCheck = 'recurring-disabled';
      }
      
      // Get the rrule object.
      // This is an instance of RRule. Refer to https://github.com/jakubroztocil/rrule on how to use it
      // console.log(rRuleFormValue.rRule.toString());
      
      // Optional - Raw value of the ngxRrule used internally
      // console.log(rRuleFormValue.raw);
    });


  }

  getCategories(){
    this.jobService.getAllCategory().subscribe(
      response => {
        this.categories = response.payload;
      },
      error => {
        console.log(error.message)
      }
    )
  }

  getClients(){
    this.clientService.getAll().subscribe(
      response => {
        this.options = response.payload;
      },
      error => {
        console.log("Get Client List Error")
      }
    )
  }

  createJobRequest(){
    let formValue = {
      "title" : this.jobForm.value.title,
      "start_date" : this.datePipe.transform(this.jobForm.value.start_date, 'Y-MM-dd HH:mm:ss'),
      "end_date" : this.datePipe.transform(this.jobForm.value.end_date, 'Y-MM-dd HH:mm:ss'),
      "location" : this.jobForm.value.location,
      "status" : this.jobForm.value.status,
      "rrule" : this.jobForm.value.rrule?.rRule?.toString(),
      "no_of_employee" :this.jobForm.value.no_of_employee,
      "paid_frequency" : this.jobForm.value.paid_frequency,
      "base_rate" : this.jobForm.value.base_rate,
      "client_id" : this.jobForm.value.client_id,
      "category_id" : this.jobForm.value.category_id,
      "note" : this.jobForm.value.note,
      "is_recurring" : this.jobForm.value.is_recurring,
      "description" : this.jobForm.value.description,
      "is_full_day_event" : 0,
      "is_accepted": 0,
      "recurring_type": this.jobForm.value.rrule?.raw?.repeat?.frequency?.toLowerCase()
    }

    this.jobService.create(formValue).subscribe(
      response => {
        console.log(response);
        this.notify.showToast("Job Request Created Successfully", "Success", "success");
        this.router.navigate(['pages/jobs/list']);
      },
      error => {
        console.log(error);
        this.notify.showToast(error.message, "Error", "danger");
      }
    );
    console.log(this.jobForm.value);
  }

  goBack(){
    this.NavigationService.back();
  }

}
