import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbWindowRef, NbWindowService } from '@nebular/theme';

import { EmployeeService, JobService, NavigationService, NotificationService } from '../../../@core/utils';

@Component({
  selector: 'app-job-assign',
  templateUrl: './job-assign.component.html',
  styleUrls: ['./job-assign.component.scss']
})
export class JobAssignComponent implements OnInit {

  jobId = null;
  jobDetail:any = {};
  employees:any = [];

  selectedEmployeeNgModel = [];
  savedEmployee = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private NavigationService: NavigationService,
    private windowService: NbWindowService,
    private notify: NotificationService,
    private jobService: JobService,
    private employeeService: EmployeeService,
  ) {

    this.jobId = this.route.snapshot.paramMap.get('id');
    this.getJobDetail(this.jobId);
  }

  ngOnInit() {
    // this.getEmployees();
    this.getEmployeesByJobId();
  }

  getJobDetail(jobId){
    this.jobService.get(jobId).subscribe(
      response => {
        console.log(response);
        this.jobDetail = response;
        // let formValue = {
        //   "title" : response.title,
        //   "start_date" : response.start_date,
        //   "end_date" : response.end_date,
        //   "location" : response.location,
        //   "status" : response.status,
        //   "rrule" : response.rrule,
        //   "no_of_employee" : response.no_of_employee,
        //   "paid_frequency" : response.paid_frequency,
        //   "base_rate" : response.base_rate,
        //   "client_id" : response.client_id,
        //   "note" : response.note,
        //   "is_recurring" : response.is_recurring,
        //   "description" : response.description,
        // }
        this.getEmployees(response.category_id);
        
      },
      error => {
        this.notify.showToast(error?.message, "Error", "danger");
      }
    )

  }

  getEmployees(category_id){
    this.employeeService.getByCategory(category_id).subscribe(
      response => {
        this.employees = response?.payload || [];
      },
      error => {
        this.notify.showToast(error?.message, "Error", "danger");
      }
    )
  }

  getEmployeesByJobId(){
    this.jobService.getEmployeebyJobId(this.jobId).subscribe(
      response => {
        this.savedEmployee = response?.payload || [];
        // let employeeArray = [];
        // for(let employee of savedEmployees){
        //   employeeArray.push(employee.id);
        // }
        this.selectedEmployeeNgModel = this.savedEmployee;
      },
      error => {
        this.notify.showToast(error?.message, "Error", "danger");
      }
    );
  }

  saveAssignedEmployee(){
    let assignedEmployee = [];
    for (let employee of this.selectedEmployeeNgModel) {
      assignedEmployee.push(employee.id);
    }

    let formData = {
      job_id : this.jobId,
      employee_id : assignedEmployee
    }

    if(assignedEmployee.length <= this.jobDetail?.no_of_employee && formData.job_id && formData.employee_id){
      this.jobService.assignEmployee(formData).subscribe(
        response => {
          this.notify.showToast("Employee assigned to job successfully", "Success", "success");
          this.router.navigate(['pages/jobs/list']);
        },
        error => {
          this.notify.showToast(error?.message, "Error", "danger");
        }
      )
    }else{
      this.notify.showToast(`you can only assign atmost ${this.jobDetail?.no_of_employee} to this job`, "Error", "danger");
    }
  }



  goBack(){
    this.NavigationService.back();
  }

}
