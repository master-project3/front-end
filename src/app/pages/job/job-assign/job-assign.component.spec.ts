/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { JobAssignComponent } from './job-assign.component';

describe('JobAssignComponent', () => {
  let component: JobAssignComponent;
  let fixture: ComponentFixture<JobAssignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobAssignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
