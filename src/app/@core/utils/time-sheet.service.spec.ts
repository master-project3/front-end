/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TimeSheetService } from './time-sheet.service';

describe('Service: TimeSheet', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimeSheetService]
    });
  });

  it('should ...', inject([TimeSheetService], (service: TimeSheetService) => {
    expect(service).toBeTruthy();
  }));
});
