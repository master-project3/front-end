import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { of as observableOf } from 'rxjs/observable/of';

import { NbAuthService, NbAuthJWTToken } from '@nebular/auth';
import { NbRoleProvider } from '@nebular/security';

@Injectable()
export class RoleProvider implements NbRoleProvider {

  constructor(private authService: NbAuthService) {
  }

  getRole(): Observable<string> {
    let user = JSON.parse(localStorage.getItem('user'));
    let role = user?.roles[0]?.name;
    let role_name;
    if(role == 'admin') role_name = "admin";
    if(role == 'service-supervisor') role_name = "service_supervisor";
    if(role == 'staff-manager') role_name = "staff_manager";
    if(role == 'payroll-officer') role_name = "payroll_officer";
    return observableOf('role_name');
  }
}