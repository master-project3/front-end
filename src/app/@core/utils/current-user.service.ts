import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

constructor() { }

logout(){
 localStorage.removeItem('user');
 localStorage.clear();
 
}

getUserData(){
  return JSON.parse(localStorage.getItem('user'));
}

getRole(){
  let user = JSON.parse(localStorage.getItem('user'));
    let role = user?.roles[0]?.name;
    let role_name;
    if(role == 'admin') role_name = "admin";
    if(role == 'service-supervisor') role_name = "service_supervisor";
    if(role == 'staff-manager') role_name = "staff_manager";
    if(role == 'payroll-officer') role_name = "payroll_officer";

  return role_name;
}

}
