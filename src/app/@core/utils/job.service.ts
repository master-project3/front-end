import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environment';

// const baseUrl = 'http://localhost:8000/api/jobs';

@Injectable({
  providedIn: 'root',
})

export class JobService {


  baseUrl = environment.baseUrl + '\jobs';

  assignUrl = environment.baseUrl + '\assign';

  categoryUrl = environment.baseUrl + '\categories';
  

  constructor( private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  get(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  
  getAllCategory(): Observable<any> {
    return this.http.get(this.categoryUrl);
  }

  getEmployeebyJobCategory(id): Observable<any> {
    return this.http.get(`${this.assignUrl}/employees?category_id=${id}`);
  }

  getJobsbyEmployeeId(id): Observable<any> {
    return this.http.get(`${this.assignUrl}/employees?employee_id=${id}`);
  }

  getEmployeebyJobId(id): Observable<any> {
    return this.http.get(`${this.assignUrl}/employees?job_id=${id}`);
  }

  assignEmployee(data): Observable<any> {
    return this.http.post(`${this.assignUrl}/employees`, data);
  }
  create(data): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

  findByName(name): Observable<any> {
    return this.http.get(`${this.baseUrl}?search_value=${name}`);
  }

}
