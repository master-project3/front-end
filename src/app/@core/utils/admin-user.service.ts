import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environment';

@Injectable({
  providedIn: 'root'
})
export class AdminUserService {

  baseUrl = environment.baseUrl + '\admin/users';

  transactionUrl = environment.baseUrl + 'transaction/history';

  constructor( private http: HttpClient, private httpBackend: HttpBackend) { }

  getAll(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  get(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(data): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

  findByName(name): Observable<any> {
    return this.http.get(`${this.baseUrl}?search_value=${name}`);
  }

  // For Moonova API Key : e91cba03-507a-458a-9119-7663fcc7abe

  getMAccountTransactions():Observable<any> {
    return this.http.get(`${this.transactionUrl}`);
  }

}
