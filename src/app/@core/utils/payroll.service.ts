import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environment';

@Injectable({
  providedIn: 'root'
})
export class PayrollService {

  baseUrl = environment.baseUrl + "payrolls";

  payUrl = environment.baseUrl + "pay/employee";

  paySlipUrl = environment.baseUrl + "payslips/histories"

  constructor( private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getPayrollHistory(employee_id): Observable<any> {
    return this.http.get(`${this.paySlipUrl}?employee_id=${employee_id}`);
  }

  get(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  payByPayslidId(data): Observable<any> {
    return this.http.post(this.payUrl, data);
  }

  getPayroll(payrollParams): Observable<any> {
    return this.http.get(`${this.baseUrl}?${payrollParams}`);
  }

  create(data): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

  findByName(name): Observable<any> {
    return this.http.get(`${this.baseUrl}?search_value=${name}`);
  }

}
