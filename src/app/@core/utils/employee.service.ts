import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  baseUrl = environment.baseUrl + '\employees';

  categoryUrl = environment.baseUrl + '\categories';

  constructor( private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getAllCategory(): Observable<any> {
    return this.http.get(this.categoryUrl);
  }


  get(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  create(data): Observable<any> {
    return this.http.post(this.baseUrl, data);
  }

  getAvailabilityByEmployee(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/avaibility?employee_id=${id}`);
  }

  createAvailability(data): Observable<any> {
    return this.http.post(`${this.baseUrl}/avaibility`, data);
  }

  update(id, data): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, data);
  }

  delete(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(this.baseUrl);
  }

  findByName(name): Observable<any> {
    return this.http.get(`${this.baseUrl}?search_value=${name}`);
  }

  getByCategory(category_id): Observable<any> {
    return this.http.get(`${this.baseUrl}?category_id=${category_id}`);
  }

  getAllSkill(): Observable<any> {
    return this.http.get(this.categoryUrl);
  }

  createSkill(data): Observable<any> {
    return this.http.post(this.categoryUrl, data);
  }

  updateSkill(id, data): Observable<any> {
    return this.http.put(`${this.categoryUrl}/${id}`, data);
  }



}
