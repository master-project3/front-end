import { Injectable } from '@angular/core';
import { NbToastrService, NbComponentStatus } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private toasterService: NbToastrService
  ) { }

  parseSuccessMessage(){

  }

  parseErrorMessage(){

  }

  showToast(message, title ,notificationType){
    this.toasterService.show(message, title, { status: notificationType, duration: 8000 });
  }

}
