import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { CurrentUserService, LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themes = [
    {
      value: 'default',
      name: 'Light',
    },
    {
      value: 'dark',
      name: 'Dark',
    },
    // {
    //   value: 'cosmic',
    //   name: 'Cosmic',
    // },
    // {
    //   value: 'corporate',
    //   name: 'Corporate',
    // },
  ];

  currentTheme = 'default';

  // userMenu = [ { title: 'Profile', fun:'' }, { title: 'Log out', fun:'logout' } ];

  userMenu:any = [];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              // private userService: UserData,
              private layoutService: LayoutService,
              private breakpointService: NbMediaBreakpointsService,
              private currentUserService: CurrentUserService,
              private router: Router,
              ) {

                menuService.onItemClick().subscribe(response => {
                  // .... do what you want
                  console.log(response);
                  if(response?.item?.title == "Log out" ){
                    this.logout();
                  }
                  if(response?.item?.title == "Edit Detail" ){
                    this.editUser();
                  }
                });
               
                this.userMenu = [ 
                  { title: `Edit Detail` },
                  { title: `Role: ${this.currentUserService.getUserData().roles[0].display_name }`},
                  { title: 'Log out', fun:'logout' }
                 ];
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;

    // Dummy Data test Service
    // this.userService.getUsers()
    //   .pipe(takeUntil(this.destroy$))
    //   .subscribe((users: any) => this.user = users.nick);
    // this.user = JSON.parse(localStorage.getItem('user'));

    this.user = this.currentUserService.getUserData();

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = themeName);
  }

  editUser(){
    this.user = this.currentUserService.getUserData();
    this.router.navigate([`pages/user/edit/${this.user.id}`]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  logout(){
    this.currentUserService.logout();
    this.router.navigate(['auth/login']);
    location.reload();
  }
}

