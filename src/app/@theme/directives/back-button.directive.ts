import { Directive, HostListener } from '@angular/core';
import { NavigationService } from '../../@core/utils/navigation.service';

@Directive({
  selector: '[ngxbackButton]',
})
export class BackButtonDirective {
  constructor(private navigation: NavigationService) {}

  @HostListener('click')
  onClick(): void {
    this.navigation.back();
  }
  
}
